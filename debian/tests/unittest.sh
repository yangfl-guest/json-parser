#!/bin/sh
set -e

cd tests
gcc -o test_json -I.. ../examples/test_json.c -lm -ljsonparser
for i in invalid-*.json; do
  if ./test_json $i > /dev/null 2>&1 ; then
    exit 1
  fi
done
for i in valid-*.json; do
  if ! ./test_json $i > /dev/null 2>&1 ; then
    exit 1
  fi
done
rm test_json
exit 0
